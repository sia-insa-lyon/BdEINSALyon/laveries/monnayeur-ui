from dataclasses import dataclass
from queue import Queue
from threading import Thread
from typing \
    import Callable


@dataclass
class TaskWithCallback:
    functionptr: Callable
    callback: Callable[[any], None]


class Callbacker(Thread):
    """
    Eh ça sert pas à rien les cours de PPC :)
    """

    def __init__(self):
        super(Callbacker, self).__init__(daemon=True)
        self.tasks: Queue[TaskWithCallback] = Queue()

    def run(self) -> None:
        while True:
            task = self.tasks.get(block=True)
            res = task.functionptr()
            task.callback(res)

    def enqueue(self, task: TaskWithCallback):
        self.tasks.put(task)
