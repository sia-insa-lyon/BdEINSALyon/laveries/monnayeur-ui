from threading import Thread
from time import sleep
from typing import Callable

import gi
import requests
from nfc_list_wrapper.get_tag import get_tag_uid, NfcScanner

from utils import Callbacker, TaskWithCallback

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GLib

builder = Gtk.Builder()
builder.add_from_file('admin_ui.glade')
main_win: Gtk.Window = builder.get_object('main_win')
main_stack: Gtk.Stack = builder.get_object('main_stack')
main_stack_switcher: Gtk.StackSwitcher = builder.get_object('main_stack_switcher')
token_uid_va: Gtk.Entry = builder.get_object('token_uid_va')
token_count: Gtk.Entry = builder.get_object('token_count')
token_add_button: Gtk.Button = builder.get_object('token_add_button')
token_add_infos: Gtk.Label = builder.get_object('token_add_infos')
network_spinner: Gtk.Spinner = builder.get_object('network_spinner')

class TagPollThread(Thread):
    def run(self) -> None:
        while True:
            tag = get_tag_uid()
            if len(tag) > 1:
                GLib.idle_add(token_add_button.set_sensitive, True)
                GLib.idle_add(token_add_button.set_label, "Ajouter les jetons")
                GLib.idle_add(token_uid_va.set_text, tag)
                sleep(2)
            sleep(0.1)





callbacker = Callbacker()


def add_token_callback(req):
    network_spinner.stop()
    token_add_button.set_sensitive(True)
    res = req.json()
    if req.status_code == 200:
        token_add_button.set_sensitive(False)
        token_add_button.set_label("Succès !")
        token_add_infos.set_text(f"{res['email']} possède désormais {res['washing_tokens_count']} jetons")
    else:
        try:
            token_add_infos.set_text(res['add_count'][0])
        except:
            pass
    print(res)


def slow(f: Callable, t: int):
    sleep(t)
    return f()


def add_token(parent=None):
    count = token_count.get_text()
    uid = token_uid_va.get_text()
    task = TaskWithCallback(functionptr=lambda: slow(
        lambda: requests.post(f"http://localhost:8000/residents/{uid}/add_washing_tokens/", data={'add_count': count}),
        2),
                            callback=add_token_callback)
    callbacker.enqueue(task)
    network_spinner.start()
    token_add_button.set_sensitive(False)


token_add_button.connect('clicked', add_token)
token_count.connect('activate', add_token)


def quit(_=None):
    exit(0)


if __name__ == '__main__':
    tpt = TagPollThread(daemon=True)
    main_win.show_all()
    main_win.connect('destroy', quit)  # pour le bouton 'fermer'
    with NfcScanner():
        tpt.start()
        callbacker.start()
        Gtk.main()
    quit()
