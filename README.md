# Installation

## Dépendances

```shell
sudo apt install libgirepository1.0-dev gcc libcairo2-dev pkg-config python3-dev gir1.2-gtk-3.0 libnfc-dev
```

Ensuite il faut télécharger le module
python [nfc_list_wrapper](https://gitlab.com/sia-insa-lyon/BdEINSALyon/laveries/libnfc-python) depuis gitlab

# Développement

Pour modifier l'agencement, le style etc... de l'interface utilisateur, il faut utiliser Glade (gratuit), pour éditer le
fichier ``ui.glade``.