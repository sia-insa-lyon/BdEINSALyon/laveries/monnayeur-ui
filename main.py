from dataclasses import dataclass
from threading import Thread, Event
from time import sleep
from typing import Union

import gi
import requests

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GLib
from nfc_list_wrapper.get_tag import get_tag_uid, NfcScanner

STATES = {
    ''
}


@dataclass
class State:
    counter = 2
    step = 0
    tag_uid = None
    machine = None
    program = None
    resident: Union[None, dict] = None


state = State()

UI_RESET_TIME = 15  # multiples de 5s avant que l'UI revienne à zéro

CONFIG = {
    'building': "1",
    'api_url': "http://localhost:8000",
    # 'api_url':"http://vps.ribes.ovh:8000",
    'machines': [],
}
pollthread_event = Event()


def reset_timer():
    state.counter = UI_RESET_TIME


def reset_pay_button():
    button_pay.set_label("Payer")
    button_pay.set_sensitive(True)


def wake_thread(_=None):
    print("waking Thread")
    pollthread_event.set()
    # nfc_poll_condition.acquire(blocking=False)


def fill_config():
    req = requests.get(CONFIG['api_url'] + '/machines/?building=' + CONFIG['building'])
    res = req.json()
    print(res)
    CONFIG['machines'] = res


fill_config()

builder = Gtk.Builder()
builder.add_from_file('ui.glade')
main_win: Gtk.Window = builder.get_object('main_window')
uid_entry: Gtk.Entry = builder.get_object('uid_entry')
email_label: Gtk.Label = builder.get_object('email_label')
notebook: Gtk.Notebook = builder.get_object('main_tabs')
machines_hbox: Gtk.Box = builder.get_object('machines_hbox')
programs_hbox: Gtk.Box = builder.get_object('programs_hbox')
button_pay: Gtk.Button = builder.get_object('button_pay')
reset_button: Gtk.Button = builder.get_object('reset_button')
login_button: Gtk.Button = builder.get_object('login_button')
payment_cost: Gtk.Label = builder.get_object('payment_cost')
payment_infos: Gtk.Label = builder.get_object('payment_infos')
reset_timer_label: Gtk.Label = builder.get_object('reset_timer_label')
main_win.set_title("Monnayeur de laveries")

email_init_text = email_label.get_text()


def show_programs(machine: dict):
    for program in machine['programs']:
        print(program['name'])
        vbox = Gtk.Box()
        vbox.set_orientation(Gtk.Orientation.VERTICAL)
        duration = Gtk.Label(label=program['duration'])
        btn = Gtk.Button(label=program['name'])
        btn.set_halign(Gtk.Align.CENTER)
        btn.set_valign(Gtk.Align.CENTER)
        btn.connect('clicked', select_program, program)
        vbox.add(duration)
        vbox.add(btn)
        programs_hbox.add(vbox)
    programs_hbox.show_all()


def select_machine(_, machine: dict):
    state.machine = machine
    reset_timer()
    print("selected machine #", machine['id'])
    GLib.idle_add(show_programs, machine)
    state.step = 2
    notebook.next_page()


def select_program(_, program: dict):
    print("selected program", program['id'])
    state.program = program
    reset_timer()
    state.step = 3

    GLib.idle_add(payment_infos.set_text, f"il vous reste {state.resident['washing_tokens_count']} jetons")
    payment_cost.set_text(f"Prix: {state.machine['cost']} jeton(s)")
    reset_pay_button()
    notebook.next_page()


for machine in CONFIG['machines']:
    print(machine['name'])
    btn: Gtk.Button = Gtk.Button(label=machine['name'])
    btn.set_halign(Gtk.Align.CENTER)
    btn.set_valign(Gtk.Align.CENTER)
    btn.connect('clicked', select_machine, machine)
    machines_hbox.add(btn)

    print('==========')
machines_hbox.show_all()


def reset_programmes():
    for ch in programs_hbox.get_children():
        programs_hbox.remove(ch)


def _ui_reset(parent=None):
    uid_entry.set_text("")
    email_label.set_text(email_init_text)
    state.step = 0
    notebook.set_current_page(0)
    reset_programmes()


def ui_reset(parent=None):
    _ui_reset(parent)
    wake_thread()


reset_button.connect('clicked', ui_reset)


def login(parent=None):
    state.step = 1
    # wake_thread()
    notebook.next_page()
    reset_timer()


login_button.connect('clicked', login)


def payment(_btn):
    print("paiement de", state.resident['email'], "pour", state.program, state.machine)
    req = requests.post(f"{CONFIG['api_url']}/machines/{state.machine['id']}/run_program/", data={
        "resident": state.resident['rfid_uid'],
        "program": state.program['id'],
    })
    res = req.json()
    print("réponse", res['operation_status'], res)
    button_pay.set_sensitive(False)
    reset_timer()
    if res['operation_status']:
        button_pay.set_label("Payé !")
        payment_infos.set_text(f"Machine lancée , il vous reste {res['resident']['washing_tokens_count']} jetons")
    else:
        button_pay.set_label("Erreur !")
        payment_infos.set_text(", ".join(res['details']))


button_pay.connect('clicked', payment)


def get_resident(tag) -> Union[dict, None]:
    try:
        req = requests.get(CONFIG['api_url'] + '/residents/' + tag + '/')
        res = req.json()
        assert res['adhesion_id']
        assert res['washing_tokens_count'] >= 0
        return res
    except:
        return None


class TagPollThread(Thread):
    def __init__(self, *args, **kwargs):
        super(TagPollThread, self).__init__(daemon=True, *args, **kwargs)

    def run(self) -> None:
        while True:
            if state.step > 0:  # on évite de poll le NFC quand l'utilisateur interagit avec l'interface
                sleep(1)
            tag = get_tag_uid()
            GLib.idle_add(uid_entry.set_text, tag)
            if tag != "":
                state.tag_uid = tag
                state.resident = get_resident(tag)
                if state.resident is not None:
                    GLib.idle_add(email_label.set_text,
                                  f"{state.resident['email']} - il vous reste {state.resident['washing_tokens_count']} jeton(s)")
                    state.counter = UI_RESET_TIME
                    while state.counter > 0:
                        GLib.idle_add(reset_timer_label.set_label, f"Reset dans {state.counter} secondes")
                        if pollthread_event.wait(timeout=1):
                            pollthread_event.clear()
                            print("interrompu !")
                            break  # pour quitter l'attente car il y a eu un reset
                        print("thread counter ping")
                        state.counter -= 1
                    print("counter=0")
                    print("too long, UI reset")
                    GLib.idle_add(_ui_reset)
                else:
                    GLib.idle_add(email_label.set_text, "Carte étudiante inconnue du BdE")
                    pollthread_event.wait(5)
            else:
                if state.tag_uid is not None and state.tag_uid != "":
                    GLib.idle_add(email_label.set_text, email_init_text)
                    sleep(1)
                state.resident = None
                sleep(0.2)


def protect_tabs(parent, tabcontent, tab_index):
    print("switch-page event", tab_index, "step=", state.step)
    if tab_index > state.step:
        GLib.idle_add(lambda: notebook.set_current_page(state.step))
        print("switching back")
    if tab_index == 1 and state.step > 1:  # on revient aux machines
        reset_programmes()
    if tab_index < 3 and state.step == 3:
        print("reset pay button")
        reset_pay_button()


notebook.connect('switch-page', protect_tabs)  # empêche les gens de sauter des étapes


def quit(_=None):
    exit(0)


if __name__ == '__main__':
    main_win.show_all()
    main_win.connect('destroy', quit)  # pour le bouton 'fermer'

    with NfcScanner():
        tpt = TagPollThread()
        tpt.start()
        Gtk.main()
    quit()
